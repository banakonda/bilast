<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//Bilet.php';

class BiletsRepository extends Repository {

    public function getBilets(string $fromTown, string $toTown): array {
        $result = [];
        $stmt = $this->database->connect()->prepare('
        SELECT * FROM bilet WHERE from_town like :fromTown AND to_town like :toTown
        ');
        $stmt->bindParam(':fromTown', $fromTown, PDO::PARAM_STR);
        $stmt->bindParam(':toTown', $toTown, PDO::PARAM_STR);
        $stmt->execute();
        $bilets = $stmt->fetchAll(PDO::FETCH_ASSOC);
        
        if($bilets == false) {
            return []; //w razie gry brak wyników
        }

        foreach ($bilets as $bilet) {
            $result[] = new Bilet(
                $bilet['ID_bilet'],
                $bilet['from_town'],
                $bilet['to_town'],
                $bilet['when_date'],
                $bilet['img_code'],
                $bilet['cost'],
            );
        }
        
        return $result;
    }

    public function makeBilet(string $from_town, string $to_town, string $when_date, string $img_code, int $cost)
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO  BILET ( from_town, to_town, when_date, img_code, cost) 
            VALUES (:from_town, :to_town, :when_date, :img_code, :cost)
            ');
        
        $stmt->bindParam(':from_town', $from_town, PDO::PARAM_STR);
        $stmt->bindParam(':to_town', $to_town, PDO::PARAM_STR);
        $stmt->bindParam(':when_date', $when_date, PDO::PARAM_STR);
        $stmt->bindParam(':img_code', $img_code, PDO::PARAM_STR);
        $stmt->bindParam(':cost', $cost, PDO::PARAM_INT);
        $stmt->execute();
    }
    
}