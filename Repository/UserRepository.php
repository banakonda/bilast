<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//User.php';

class UserRepository extends Repository {

    public function getUser(string $email): ?User 
    {
        
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM user WHERE email = :email
        ');

        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false) {
            return null;
        }

        return new User(
            $user['ID_user'],
            $user['email'],
            $user['password'],
            $user['points'],
            $user['QR_code'],
            $user['ID_pref'],
            $user['ID_role']
        );
    }

    public function getUsers(): array {


        try {
            $stmt = $this->database->connect()->prepare('SELECT * FROM user WHERE email != :email;');
            $stmt->bindParam(':email', $_SESSION['email'], PDO::PARAM_STR);
            $stmt->execute();
            $user = $stmt->fetchAll(PDO::FETCH_ASSOC);
            return $user;
        }
        catch(PDOException $e) {
            die();
        }

    }
    
    public function makeUser(string $email, string $password, int $points, $QR_code, $ID_pref, $ID_role)
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO  USER ( email, password, points, QR_code, ID_pref, ID_role) 
            VALUES (:email, :password, :points, :QR_code, :ID_pref, :ID_role)
            ');
        $stmt->bindParam(':email', $email, PDO::PARAM_STR);
        $stmt->bindParam(':password', $password, PDO::PARAM_STR);
        $stmt->bindParam(':points', $points, PDO::PARAM_INT);
        $stmt->bindParam(':QR_code', $QR_code, PDO::PARAM_INT);
        $stmt->bindParam(':ID_pref', $ID_pref, PDO::PARAM_INT);
        $stmt->bindParam(':ID_role', $ID_role, PDO::PARAM_INT);
        $stmt->execute();
    }
}
