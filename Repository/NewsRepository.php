<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//News.php';

class NewsRepository extends Repository {

    public function getNews(): array{
        $result = [];
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM news 
            ORDER BY ID_news DESC
            LIMIT 3
        ');

        $stmt->execute();
        $news = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if($news == false) {
            return null;
        }

        foreach ($news as $not) {
            $result[] = new News(
                $not['ID_news'],
                $not['title'],
                $not['txt']
            );
        }
        
        return $result;
    }

    public function makeNews(string $title, string $txt)
    {
        $stmt = $this->database->connect()->prepare('
            INSERT INTO NEWS ( title, txt) 
            VALUES (:title, :txt)
            ');
        $stmt->bindParam(':title', $title, PDO::PARAM_STR);
        $stmt->bindParam(':txt', $txt, PDO::PARAM_STR);

        $stmt->execute();
    }

    
}
