<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class AdminController extends AppController {

    public function index()
    {
        $userRepository = new UserRepository();
        $this->render('users', ['user' => $userRepository->getUser($_SESSION['email'])]);
    }

    public function users()
    {   
        
        $userRepository = new UserRepository();
        
        header('Content-type: application/json');
        http_response_code(200);

        $users = $userRepository->getUsers();
        echo $users ? json_encode($users) : '';
    }

    public function mNews(){
        if ($this->isPost()) {  
            $newsRepository = new NewsRepository();
            $title = $_POST['title'];
            $txt = $_POST['txt'];

            if (empty($title) || empty($txt)) {
                $this->render('admin-news', ['messages' => ['Uzupełnij wszystkie pola!']]);
                return;
            }

            $newsRepository->makeNews($title, $txt);
            $this->render('admin-news', ['messages' => ['Newsa dodano pomyślnie!']]);

        } else {
            $this->render('admin-news');
        }
    }
}