<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Database.php';
require_once __DIR__.'//..//Repository//NewsRepository.php';
require_once __DIR__.'//..//Repository//BiletsRepository.php';


class BoardController extends AppController {

    public function getSearchReady(){
        if ($this->isPost()) {
            $biletsRepository = new BiletsRepository();
            $from_town = $_POST['from_town'];
            $to_town = $_POST['to_town'];
            $bilet = $biletsRepository->getBilets($from_town, $to_town);

            if(!$bilet){

                $this->render('search', ['messages' => ['Brak pasujących biletów']]);
            } else {
                $this->render('results', ['bilets' => $biletsRepository->getBilets($from_town, $to_town)]);
            }
        } else {
        $this->render('search');
        }
    }

    public function getSettings(){
        $this->render('settings');
    }

    public function getNews(){
        $newsRepository = new NewsRepository();
        $newsRepository->getNews();
        $this->render('notification', ['news' => $newsRepository->getNews()]);
    }

    public function getBilets(){
        $this->render('my-bilets');
    }

    public function addBilets(){
        if ($this->isPost()) {
            $biletsRepository = new BiletsRepository();
            $from_town = $_POST['from_town'];
            $to_town = $_POST['to_town'];
            $when_date = $_POST['when_date'];
            $img_code = $_POST['img_code'];
            
            if (empty($from_town) || empty($to_town) || empty($when_date) || empty($img_code)) {
                $this->render('add-bilet', ['messages' => ['Wszystkie pola muszą być uzupełnione!']]);
                return;
            }

            $bilet = $biletsRepository->makeBilet($from_town, $to_town, $when_date, $img_code, 0);

            $this->render('add-bilet', ['messages' => ['Dodano bilet pomyślnie!']]);
                return;
            // $url = "http://$_SERVER[HTTP_HOST]/";
            // header("Location: {$url}/bilast-cont/bilast/?page=login");

        } else {
            $this->render('add-bilet');
        }
    }


    // $userRepository = new UserRepository();

    //     if ($this->isPost()) {            
    //         $email = $_POST['email'];
    //         $password = $_POST['password'];

    //         if (empty($email)) {
    //             $this->render('register', ['messages' => ['Email jest wymagany!']]);
    //             return;
    //         }

    //         if (empty($password)) {
    //             $this->render('register', ['messages' => ['Haslo jest wymagane!']]);
    //             return;
    //         }

    //         $user = $userRepository->getUser($email);

    //         if ($user) {
    //             $this->render('register', ['messages' => ['Użytkownik o takim adresie email już istnieje!']]);
    //             return;
    //         }

    //         $hashed_pass = password_hash($password, PASSWORD_DEFAULT);
    //         $user = $userRepository->makeUser($email, $hashed_pass, 0, 0, 1, 1);

    //         $url = "http://$_SERVER[HTTP_HOST]/";
    //         header("Location: {$url}/bilast/Strona/?page=login");
    //         return;
    //     }
    //     $this->render('register');
    // }
    
}