<?php

require_once 'AppController.php';
require_once __DIR__.'//..//Models//User.php';
require_once __DIR__.'//..//Repository//UserRepository.php';

class SecurityController extends AppController {

    public function login()
    {         
        $userRepository = new UserRepository();
        if ($this->isPost()) {
            
            $email = $_POST['email'];
            $password = $_POST['password'];
            $user = $userRepository->getUser($email);
            
            if(!$user) {
                $this->render('login', ['messages' => ['User with this email not exist!']]);
                return;
            }
            
            if(!password_verify($password, $user->getPassword())){
                $this->render('login', ['messages' => ['Wrong password!']]);
                return;
            }

            $_SESSION["id"] = $user->getId();
            $_SESSION["email"] = $user->getEmail();
            $_SESSION["password"] = $user->getPassword();
            $_SESSION["points"] = $user->getPoints();
            $_SESSION["QR_code"] = $user->getQR_code();
            $_SESSION["ID_pref"] = $user->getPref();
            $_SESSION["ID_role"] = $user->getRole();

            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}/bilast-cont/bilast/?page=search");
            return;
        }

        $this->render('login');
    }

    public function logout()
    {
        session_unset();
        session_destroy();

        $this->render('login', ['messages' => ['You have been successfully logged out!']]);
    }

    public function register(){
        $userRepository = new UserRepository();

        if ($this->isPost()) {            
            $email = $_POST['email'];
            $password = $_POST['password'];

            if (empty($email)) {
                $this->render('register', ['messages' => ['Email jest wymagany!']]);
                return;
            }

            if (empty($password)) {
                $this->render('register', ['messages' => ['Haslo jest wymagane!']]);
                return;
            }

            $user = $userRepository->getUser($email);

            if ($user) {
                $this->render('register', ['messages' => ['Użytkownik o takim adresie email już istnieje!']]);
                return;
            }

            $hashed_pass = password_hash($password, PASSWORD_DEFAULT);
            $user = $userRepository->makeUser($email, $hashed_pass, 0, 0, 1, 1);

            $url = "http://$_SERVER[HTTP_HOST]/";
            header("Location: {$url}/bilast-cont/bilast/?page=login");
            return;
        }
        $this->render('register');
    }

}