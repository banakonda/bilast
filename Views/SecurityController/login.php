<!DOCTYPE HTML>
<html lang="pl">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

        <title>bilast</title>      
        <link rel="Stylesheet" type="text/css" href="Public/css/login.css" />
        <script src="https://kit.fontawesome.com/17f45e8d95.js" crossorigin="anonymous"></script>
    </head>

    <body>
        <div class="container">
            <div class="logo">
                <img src="Public/img/logo.svg" />
                <img src="Public/img/bilast.svg" />
            </div>

            <form action="?page=login" method="POST">

                <div class="messages">
                    <?php
                        if(isset($messages)){
                            foreach($messages as $message) {
                                echo $message;
                            }
                        }
                    ?>
                </div>

                <div class="inputs">   
                    <div class="input-field">
                        <i class="fas fa-user"></i>
                        <input name="email" type="text" placeholder="email@email.com"  autocomplete="off"/>
                    </div>
                    
                    
                    <div class="input-field">
                        <i class="fas fa-lock"></i>
                        <input name="password" type="password" placeholder="······"  autocomplete="off"/>
                    </div>
                </div>

                <button type="submit">DALEJ</button>
            </form>
            
            <div class="media">
                <a href="#"><img src="Public/img/facebook.svg" /></a>
                <a href="#"><img src="Public/img/twitter.svg" /></a>
                <a href="#"><img src="Public/img/g+.svg" /></a>
            </div>

            <div class="register">
                    <a href="?page=register">Zarejestruj się</a> | <a href="#">Zapomniałem hasła</a>
            </div>
        </div>
        
    </body>
</html>