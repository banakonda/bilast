<!DOCTYPE HTML>
<html lang="pl">
    <head>
        <?php include(dirname(__DIR__).'../Common/head.php'); ?>

        <link rel="Stylesheet" type="text/css" href="Public/css/register.css" />
    </head>

    <body>
        <div class="container">
            <div class="register-box">

            <h2>Zarejestruj się</h2>

            <div class="mess">
                <?php
                    if (isset($messages)) {
                        foreach ($messages as $message) {
                            echo $message;
                        }
                    }
                ?>
            </div>

            <form action="?page=register" method="POST">
                <div class="input-field">
                    <div class="ico"><i class="fas fa-envelope"></i></div>
                    <input name="email" type="text" placeholder="email@email.com"  autocomplete="off"/>
                </div>
                <div class="input-field">
                    <div class="ico"><i class="fas fa-lock"></i></div>
                    <input name="password" type="password" placeholder="······"  autocomplete="off"/>
                </div>
                
                

                <button type="submit">DALEJ</button>
            </form>

            </div>
        </div>
    </body>
</html>