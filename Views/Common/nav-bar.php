<nav>
    <div class="logo">
        <img src="Public/img/logo.svg" />
        <img src="Public/img/bilast.svg" />
    </div>
        
    <button class="side-menu-trigger"><i class="fas fa-bars"></i></button>
    
    <aside class="humburger-menu">
        <aside class="side-top">
            <div class="small-dott">  
                <i class="fas fa-user"></i>
            </div>
            <div class="top-nickname">
                <h3 class='nav-nickname'>
                    <?php echo($_SESSION['email']);?>
                </h3>
                <h5>
                    Punkty: 
                    <strong class='nav-points'>
                        <?php echo($_SESSION['points']);?>
                    </strong>
                </h5>
            </div>
        </aside>
        <aside class="side-bottom">
            <div class="list">
                <a class="big-dott" href="?page=notification">
                    <i class="fas fa-bell"></i>             <!-- POWIADOMIENIA -->
                </a>

                <a class="big-dott" href="?page=add-bilet">
                <i class="fas fa-ticket-alt"></i>           <!-- MOJE BILETY -->
                </a>

                <a class="big-dott" href="?page=admin">
                    <i class="fas fa-envelope"></i>         <!-- SUPPORT -->
                </a>

                <a class="big-dott" href="?page=search">      <!-- SZUKAJ -->
                    <i class="fas fa-search"></i>
                </a>

                <a class="big-dott" href="?page=settings">    <!-- SETTINGSY -->
                    <i class="fas fa-cog"></i>
                </a>

                <a class="big-dott" href="?page=logout">    <!-- LOGOUT -->
                    <i class="fas fa-sign-out-alt"></i>
                </a>
            </div>
        </aside>
    </aside>


</nav>