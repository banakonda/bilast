<!DOCTYPE HTML>
<html lang="pl">
    <head>
        <?php include(dirname(__DIR__).'../Common/head.php'); ?>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
        <script async src="Public/js/results.js"></script>
    </head>

    <body>
        <?php include(dirname(__DIR__).'../Common/nav-bar.php'); ?>
            <h1>Wyniki wyszukiwań</h1> 
        <div class="container">

            <?php foreach ($bilets as $item): ?>

                <a href="#" class="infoboxx"><div class="infobox">
                    <h2><?=$item->getFromTown() ?> -> <?=$item->getToTown() ?></h2>
                    <h4><?=$item->getWhenDate() ?></h4>
                    <h4 class='code'><?=$item->getImgCode() ?></h4>
                </div></a>

            <?php endforeach ?>
        </div>
    
        <?php include(dirname(__DIR__).'../Common/footer.php'); ?>
    </body>
</html>