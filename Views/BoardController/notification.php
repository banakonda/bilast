<!DOCTYPE HTML>
<html lang="pl">
    <head>
        <?php include(dirname(__DIR__).'../Common/head.php'); ?>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
    </head>

    <body>
        <?php include(dirname(__DIR__).'../Common/nav-bar.php'); ?>
            
            <div class="container">
            
                <h1>Newsy</h1>
                <?php 
                    if($_SESSION['ID_role'] === 2){
                        echo("<p><a href='?page=admin-news' style='curosr:pointer; font-weight: 700'>Zarządzaj Newsami</a></p>");
                    }
                ?>
                <article class="noti">

                <?php foreach ($news as $item): ?>

                <h3>
                    <?=$item->getTitle() ?>
                </h3>
                <p>
                    <?=$item->getTxt() ?>
                </p>

                <?php endforeach ?>
                </article>

                
            </div>


        <?php include(dirname(__DIR__).'../Common/footer.php'); ?>
    </body>
</html>