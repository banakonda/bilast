<!DOCTYPE HTML>
<html lang="pl">
    <head>
        <?php include(dirname(__DIR__).'../Common/head.php'); ?>
    </head>

    <body>
        <?php include(dirname(__DIR__).'../Common/nav-bar.php'); ?>
        
        <div class="container">
            <h1>Wyszukaj bilet!</h1>
            <div class="messages">
                    <?php
                        if(isset($messages)){
                            foreach($messages as $message) {
                                echo $message;
                            }
                        }
                    ?>
            </div>
            <form action="?page=search" method="POST">
                <div class="one-line">
                    <div class="input-field">
                        <div class="input-sq-s">Z</div>
                        <input name="from_town" type="text" placeholder="Kraków Główny" autocomplete="off"/>
                    </div>

                    <div class="input-field">
                        <div class="input-sq-s">DO</div>
                        <input name="to_town" type="text" placeholder="Wrocław Główny" autocomplete="off"/>
                    </div>
                </div>

                <button type="submit">SZUKAJ</button>
            </form>
        </div>
        
        <?php include(dirname(__DIR__).'../Common/footer.php'); ?>
    </body>
</html>