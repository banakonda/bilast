<!DOCTYPE HTML>
<html lang="pl">
    <head>
        <?php include(dirname(__DIR__).'../Common/head.php'); ?>
    </head>

    <body>
        <?php include(dirname(__DIR__).'../Common/nav-bar.php'); ?>
            
        <div class="container">

            <h1>Twoje bilety</h1>

            <a href="#" class="infoboxx"><div class="infobox">
                <h2>Kraków Główny -> Wrocław Główny</h2>
                <h4>1.05.2019, 17:15</h4>
            </div></a>
            <a href="#" class="infoboxx"><div class="infobox">
                <h2>Rzeszów Główny -> Szczecin Główny</h2>
                <h4>17.10.2020, 9:45</h4>
            </div></a>

            <p><a href=#>Ustawienia</a> | <a href='?page=add-bilet'>Oddaj bilet</a> | <a href=#>Szukaj biletów</a></p>

        </div>

        <?php include(dirname(__DIR__).'../Common/footer.php'); ?>
    </body>
</html>