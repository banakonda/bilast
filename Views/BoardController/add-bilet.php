<!DOCTYPE HTML>
<html lang="pl">
    <head>
        <?php include(dirname(__DIR__).'../Common/head.php'); ?>
    </head>

    <body>
        <?php include(dirname(__DIR__).'../Common/nav-bar.php'); ?>

        <div class="container">

            <h1>Oddaj bilety</h1>
                <div class="messages">
                    <?php
                        if(isset($messages)){
                            foreach($messages as $message) {
                                echo $message;
                            }
                        }
                    ?>
                </div>
            <form action="?page=add-bilet" method="POST">

                <div class='one-line'>

                    <div class="input-field">
                        <div class="input-sq-s">Z</div>
                        <input name="from_town" type="text" placeholder="np. Kraków" autocomplete="off"/>
                    </div>

                    <div class="input-field">
                        <div class="input-sq-s">DO</div>
                        <input name="to_town" type="text" placeholder="np. Wrocław" autocomplete="off"/>
                    </div>

                </div>

                 <div class='one-line'>

                    <div class="input-field">
                        <div class="input-sq-s">DATA</div>
                        <input name="when_date" type="text" placeholder="np. 20.02.2020 - 19:35" autocomplete="off"/>
                    </div>

                    <div class="input-field">
                        <div class="input-sq-s">KOD</div>
                        <input name="img_code" type="text" placeholder="np. 985562736" autocomplete="off"/>
                    </div>
                    
                </div>


                <button type="submit">ODDAJ</button>
            </form>
        </div>

        <?php include(dirname(__DIR__).'../Common/footer.php'); ?>
    </body>
</html>