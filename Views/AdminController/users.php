<?php

    if(!isset($_SESSION['id']) and !isset($_SESSION['role'])) {
        die('You are not logged in!');
    }   

    if($_SESSION['ID_role'] === 1) {
        echo('Click here to back: <a href="?page=search">LINK</a><br>');
        die('You do not have permission to watch this page!');
    }
    
?>

<!DOCTYPE HTML>
<html lang="pl">
    <head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <?php include(dirname(__DIR__).'../Common/head.php'); ?>
        <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.6/umd/popper.min.js"></script>
        <script src="Public/js/admin.js"></script>
    </head>

    <body>
    
        <?php include(dirname(__DIR__).'../Common/nav-bar.php'); ?>
        <table class="table mt-4 text-dark">
        <thead>
            <tr>
            <th scope="col">#</th>
            <th scope="col">Email</th>
            <th scope="col">Password</th>
            <th scope="col">Points</th>
            </tr>
        </thead>
        <tbody>
            <tr>
            <th scope="row"><?= $user->getId(); ?></th>
            <td><?= $user->getEmail(); ?></td>
            <td><?= $user->getPassword(); ?></td>
            <td><?= $user->getPoints(); ?></td>
            </tr>
        </tbody>
        <tbody class="users-list">
        </tbody>
    </table>

        <button class="btn-primary btn-lg ml-0" type="button" onclick="getUsers()">
            Get all users    
        </button>

        <?php include(dirname(__DIR__).'../Common/footer.php'); ?>

        
    </body>
</html>