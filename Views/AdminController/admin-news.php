<!DOCTYPE HTML>
<html lang="pl">
    <head>
        <?php include(dirname(__DIR__).'../Common/head.php'); ?>
    </head>

    <body>
        <?php include(dirname(__DIR__).'../Common/nav-bar.php'); ?>

        <div class="container">
            <h1>Dodaj Newsa!</h1>
            <div class="messages">
                    <?php
                        if(isset($messages)){
                            foreach($messages as $message) {
                                echo $message;
                            }
                        }
                    ?>
            </div>
            <form action="?page=admin-news" method="POST">

                    <input name='title' type="text" placeholder="Tytuł"  autocomplete="off">
                    <textarea name='txt' type="text" placeholder="Treść newsa"  autocomplete="off" style='height: 10em'></textarea>
                <button type="submit">Dodaj</button>

            </form>
        </div>

        <?php include(dirname(__DIR__).'../Common/footer.php'); ?>
    </body>
</html>