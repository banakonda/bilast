<?php

class News {
    private $ID_news;
    private $title;
    private $txt;


    public function __construct(
        int $ID_news = null,
        string $title,
        string $txt
        
    ) {
        $this->title = $title;
        $this->txt = $txt;
        $this->ID_news = $ID_news;
    }

    public function getID_news() :int {
        return $this->ID_news;
    }

    public function getTitle() :string {
        return $this->title;
    }
    public function getTxt() :string {
        return $this->txt;
    }
    
}