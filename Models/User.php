<?php

class User {
    private $ID_user;
    private $email;
    private $PASSWORD;
    private $points;
    private $QR_code;
    private $ID_pref;
    private $ID_role;


    public function __construct(
        int $ID_user = null,
        string $email,
        string $PASSWORD,
        int $points,
        int $QR_code,
        int $ID_pref,
        int $ID_role
        
    ) {
        $this->email = $email;
        $this->PASSWORD= $PASSWORD;
        $this->points = $points;
        $this->QR_code = $QR_code;
        $this->ID_pref = $ID_pref;
        $this->ID_role = $ID_role;
        $this->ID_user = $ID_user;
    }

    public function getId() :int {
        return $this->ID_user;
    }
    public function getEmail() :string {
        return $this->email;
    }
    public function getPassword() :string{
        return $this->PASSWORD;
    }
    public function getPoints() :int{
        return $this->points;
    }
    public function getQR_code() :int{
        return $this->QR_code;
    }
    public function getPref() :int{
        return $this->ID_pref;
    }
    public function getRole() :int{
        return $this->ID_role;
    }
}