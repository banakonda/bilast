<?php

class Bilet {
    private $ID_bilet;
    private $from_town;
    private $to_town;
    private $when_date;
    private $img_code;
    private $cost;

    public function __construct(
        int $ID_bilet = NULL,
        string $from_town,
        string $to_town,
        string $when_date,
        string $img_code,
        int $cost
    ) {
        $this->from_town = $from_town;
        $this->to_town = $to_town;
        $this->when_date = $when_date;
        $this->img_code = $img_code;
        $this->cost = $cost;
        $this->ID_bilet = $ID_bilet;
    }

    public function getID_bilet() :int {
        return $this->ID_bilet;
    }
    public function getFromTown() :string {
        return $this->from_town;
    }
    public function getToTown() :string {
        return $this->to_town;
    }
    public function getWhenDate() :string {
        return $this->when_date;
    }
    public function getImgCode() :string {
        return $this->img_code;
    }
    public function getCost() :int {
        return $this->cost;
    }
}